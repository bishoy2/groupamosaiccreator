﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;
using GroupAMosaicCreator.FileInputOutput;
using GroupAMosaicCreator.Model;
using GroupAMosaicCreator.Utilities;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GroupAMosaicCreator
{
    /// <summary>
    ///  Main page for mosaic application.
    /// </summary>
    public sealed partial class MainPage
    {
        #region Data members

        private const double ApplicationHeight = 600;
        private const double ApplicationWidth = 800;

        private readonly FileIo fileIo;
        private readonly List<Rectangle> gridLines;
        private readonly List<Line> triangleDiagonals;
        private WriteableBitmap sourceWriteableBitmap;
        private WriteableBitmap mosaicImage;
        private int blockSize;
        private Mosaic mosaic;
        private byte[] sourceImageBytes;
        private List<byte[]> paletteImageBytes;
        private readonly List<WriteableBitmap> paletteImages;

        private bool sourceImageChanged;
        private bool paletteChanged;
        private bool gridSizeChanged;
        private bool mosaicTypeChanged;
        
        private bool isFirstTimeRunning;

        private string currentMosaicType;

        private readonly string[] validMosaicTypes;

        private int numberOfPaletteImages;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the source file image.
        /// </summary>
        /// <value>
        ///     The source file image.
        /// </value>
        public StorageFile SourceFileImage { get; set; }

        /// <summary>
        ///     Gets or sets the source file palette.
        /// </summary>
        /// <value>
        ///     The source file palette.
        /// </value>
        public IReadOnlyList<StorageFile> SourceFilePalette { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainPage" /> class.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size { Width = ApplicationWidth, Height = ApplicationHeight };
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
            
            this.paletteImages = new List<WriteableBitmap>();
            this.fileIo = new FileIo();
            this.gridLines = new List<Rectangle>();
            this.paletteImages = new List<WriteableBitmap>();

            this.triangleDiagonals = new List<Line>();
            this.initializeChangedBools();
            this.isFirstTimeRunning = true;
            var timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 1)};
            timer.Tick += this.shouldGenerateMosaicButtonBeEnabled;
            timer.Start();
            this.validMosaicTypes = new[] {"defaultSelection", "solidBlock", "picture"};
            this.currentMosaicType = this.validMosaicTypes[0];
        }

        #endregion

        #region Methods

        private void shouldGenerateMosaicButtonBeEnabled(object sender, object o)
        {
            if (this.isFirstTimeRunning)
            {
                this.handleFirstMosaicGeneration();
            }
            else
            {
                this.handleSubsequentGenerations();
            }
        }

        private void handleSubsequentGenerations()
        {
            if (this.currentMosaicType == this.validMosaicTypes[1])
            {
                this.generateMosaicButton.IsEnabled = this.sourceImageChanged || this.gridSizeChanged ||
                                                      this.mosaicTypeChanged;
            }
            else if (this.currentMosaicType == this.validMosaicTypes[2])
            {
                this.generateMosaicButton.IsEnabled = this.sourceImageChanged || this.gridSizeChanged ||
                                                      this.mosaicTypeChanged || this.paletteChanged;
            }
            else
            {
                this.generateMosaicButton.IsEnabled = false;
            }
        }

        private void handleFirstMosaicGeneration()
        {
            if (this.currentMosaicType == this.validMosaicTypes[1])
            {
                this.generateMosaicButton.IsEnabled = this.sourceImageChanged && this.gridSizeChanged &&
                                                      this.mosaicTypeChanged;
            }
            else if (this.currentMosaicType == this.validMosaicTypes[2])
            {
                this.generateMosaicButton.IsEnabled = this.sourceImageChanged && this.gridSizeChanged &&
                                                      this.mosaicTypeChanged && this.paletteChanged;
            }
        }

        private void initializeChangedBools()
        {
            this.sourceImageChanged = false;
            this.paletteChanged = false;
            this.gridSizeChanged = false;
            this.mosaicTypeChanged = false;
            this.generateMosaicButton.IsEnabled = false;
        }

        private async void OpenFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                this.SourceFileImage = await this.fileIo.LoadImageFile();
                this.sourceImageBytes = await ImageConverter.ConvertFileToByteArray(this.SourceFileImage);
                this.sourceWriteableBitmap =
                    await ImageConverter.ConvertFileToWritableBitMap(this.SourceFileImage);

                this.sourceImage.Source = this.sourceWriteableBitmap;
                this.sourceImageChanged = true;

                this.sourceImageWithZoom.Source = this.sourceWriteableBitmap;
                this.sourceImageWithZoom.Height = this.sourceWriteableBitmap.PixelHeight;
                this.sourceImageWithZoom.Width = this.sourceWriteableBitmap.PixelWidth;
                this.refreshSourceZoom();
                if (this.gridCheckBox.IsChecked == true)
                {
                    this.setGridImageSourceAndDrawGrid();
                }
            }
            catch (NullReferenceException)
            {
                var invalidDialog = new MessageDialog("Image Selection Canceled");
                await invalidDialog.ShowAsync();
            }
            catch (Exception)
            {
                var invalidDialog = new MessageDialog("Invalid source image.");
                await invalidDialog.ShowAsync();
            }
        }

        private async void OpenPaletteFilesButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.paletteImages.Clear();
            this.numberOfPaletteImages = 0;
            await this.addPicturesToPalette(this.SourceFilePalette);
            this.numberOfPaletteImagesTextBlock.Text = this.numberOfPaletteImages + " Images.";
        }

        // ReSharper disable once RedundantAssignment
        private async Task addPicturesToPalette(IReadOnlyList<StorageFile> sourceFolder)
        {
            try
            {
                sourceFolder = await this.fileIo.LoadCollectionOfImageFiles();
                this.paletteImageBytes = new List<byte[]>();

                foreach (var storageFile in sourceFolder)
                {
                    var imageBytes = await ImageConverter.ConvertFileToByteArray(storageFile);
                    this.paletteImageBytes.Add(imageBytes);

                    this.paletteImages.Add(await ImageConverter.ConvertFileToWritableBitMap(storageFile));
                }
                this.imagePaletteList.ItemsSource = this.paletteImages;
                this.paletteChanged = true;
                this.addToPalette.IsEnabled = true;
                this.clearPalette.IsEnabled = true;
                this.numberOfPaletteImages += sourceFolder.Count;
            }
            catch (NullReferenceException)
            {
                await new MessageDialog("Palette Selection Canceled").ShowAsync();
            }
        }

        private async void SaveMosaicButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var mosaicBytes = await ImageConverter.GetPixelsFromWriteableBitmap(this.mosaicImage);
                this.fileIo.SaveImageFile(mosaicBytes, this.SourceFileImage);
            }
            catch (NullReferenceException)
            {
                await new MessageDialog("There is no image to save.").ShowAsync();
            }
        }

        private void GridCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var isChecked = this.gridCheckBox.IsChecked;
            if (isChecked != null && (bool) isChecked)
            {
                this.setGridImageSourceAndDrawGrid();
                this.triangleCheckBox.IsEnabled = true;
            }
            else if (isChecked == null || (bool) !isChecked)
            {
                this.gridImageHandler();
            }
        }

        private void gridImageHandler()
        {
            this.gridCheckBox.IsChecked = false;
            this.sourceImageWithGrid.Source = null;
            this.triangleCheckBox.IsEnabled = false;
            this.triangleCheckBox.IsChecked = false;
            this.switchDirectionCheckBox.IsChecked = false;
            this.switchDirectionCheckBox.IsEnabled = false;
            this.removeOldTriangleDiagonals();
            this.removeOldGridLines();
        }

        private async void setGridImageSourceAndDrawGrid()
        {
            try
            {
                this.sourceImageWithGrid.Source = await ImageConverter.ConvertFileToWritableBitMap(this.SourceFileImage);
                this.drawGrid();
            }
            catch (NullReferenceException)
            {
                await new MessageDialog("Please select source image first.").ShowAsync();
                this.gridImageHandler();
            }
        }

        private void GridSizeInput_OnTextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            this.removeOldGridLines();
            this.removeOldTriangleDiagonals();
            var containsAnythingOtherThanNumber = !this.gridSizeInput.Text.Any(char.IsNumber);
            if (containsAnythingOtherThanNumber)
            {
                var newValue = Regex.Replace(this.gridSizeInput.Text, @"[^\d]", "");
                this.gridSizeInput.Text = newValue;
            }
            else
            {
                this.drawGrid();
                this.gridSizeChanged = true;
            }
        }

        private void drawGrid()
        {
            this.removeOldGridLines();
            this.removeOldTriangleDiagonals();
            if (this.gridSizeInput.Text != "")
            {
                this.blockSize = int.Parse(this.gridSizeInput.Text);
                var offset = this.blockSize;
                for (var i = 1; i < this.sourceImageWithGrid.Width/offset; i++)
                {
                    this.placeNormalGridLine(offset * i);
                }
                var numberOfDiagonals = (this.sourceImageWithGrid.Width / this.blockSize) * 2 - 1;
                var multiplier = 0;
                for (var i = 1; i < numberOfDiagonals / 2 + 1; i++)
                {
                    this.placeUpperTriangleGridLine(offset * i);
                    multiplier++;
                }
                for (var i = 1; i < multiplier; i++)
                {
                    this.placeLowerTriangleGridLine(offset * i);
                }
            }
        }

        private void placeNormalGridLine(int offset)
        {
            var lineX = new Rectangle
            {
                Height = .75,
                Width = this.sourceImageWithGrid.Width,
                Fill = new SolidColorBrush(Colors.White)
            };
            var lineY = new Rectangle
            {
                Height = this.sourceImageWithGrid.Height,
                Width = .75,
                Fill = new SolidColorBrush(Colors.White)
            };
            Canvas.SetLeft(lineX, Canvas.GetLeft(this.sourceImageWithGrid));
            Canvas.SetTop(lineX, Canvas.GetTop(this.sourceImageWithGrid) + offset);
            this.theCanvas.Children.Add(lineX);
            this.gridLines.Add(lineX);

            Canvas.SetLeft(lineY, Canvas.GetLeft(this.sourceImageWithGrid) + offset);
            Canvas.SetTop(lineY, Canvas.GetTop(this.sourceImageWithGrid));
            this.theCanvas.Children.Add(lineY);
            this.gridLines.Add(lineY);
        }

        private void placeUpperTriangleGridLine(int offset)
        {
            var triangles = this.triangleCheckBox.IsChecked;
            if (triangles != null && (bool)triangles)
            {
                var diagonal = new Line
                {
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = .75
                };
                var switchDiagonals = this.switchDirectionCheckBox.IsChecked;
                if (switchDiagonals != null && (bool)switchDiagonals)
                {
                    Canvas.SetLeft(diagonal, Canvas.GetLeft(this.sourceImageWithGrid) + this.sourceImageWithGrid.Width);
                    Canvas.SetTop(diagonal, Canvas.GetTop(this.sourceImageWithGrid));
                    diagonal.X1 = 0;
                    diagonal.X2 = -offset;
                    diagonal.Y1 = offset;
                    diagonal.Y2 = 0;
                }
                else
                {
                    Canvas.SetLeft(diagonal, Canvas.GetLeft(this.sourceImageWithGrid));
                    Canvas.SetTop(diagonal, Canvas.GetTop(this.sourceImageWithGrid));
                    diagonal.X1 = 0;
                    diagonal.X2 = offset;
                    diagonal.Y1 = offset;
                    diagonal.Y2 = 0;
                }
                this.theCanvas.Children.Add(diagonal);
                this.triangleDiagonals.Add(diagonal);
            }
        }

        private void placeLowerTriangleGridLine(int offset)
        {
            var triangles = this.triangleCheckBox.IsChecked;
            if (triangles != null && (bool)triangles)
            {
                var diagonal = new Line
                {
                    Stroke = new SolidColorBrush(Colors.White),
                    StrokeThickness = .75
                };
                var switchDiagonals = this.switchDirectionCheckBox.IsChecked;
                if (switchDiagonals != null && (bool)switchDiagonals)
                {
                    Canvas.SetLeft(diagonal, Canvas.GetLeft(this.sourceImageWithGrid));
                    Canvas.SetTop(diagonal, Canvas.GetTop(this.sourceImageWithGrid) + this.sourceImageWithGrid.Height);
                    diagonal.X1 = offset;
                    diagonal.X2 = this.sourceImageWithGrid.Width / offset;
                    diagonal.Y1 = this.sourceImageWithGrid.Height / offset;
                    diagonal.Y2 = -offset;
                }
                else
                {
                    Canvas.SetLeft(diagonal, Canvas.GetLeft(this.sourceImageWithGrid) + this.sourceImageWithGrid.Width / offset);
                    Canvas.SetTop(diagonal, Canvas.GetTop(this.sourceImageWithGrid) + this.sourceImageWithGrid.Width / offset);
                    diagonal.X1 = offset;
                    diagonal.X2 = this.sourceImageWithGrid.Width;
                    diagonal.Y1 = this.sourceImageWithGrid.Height;
                    diagonal.Y2 = offset;
                }
                this.triangleDiagonals.Add(diagonal);
                this.theCanvas.Children.Add(diagonal);
            }
        }

        private void removeOldGridLines()
        {
            foreach (var rectangle in this.gridLines)
            {
                this.theCanvas.Children.Remove(rectangle);
            }
            this.gridLines.Clear();
        }

        private void removeOldTriangleDiagonals()
        {
            foreach (var triangleDiagonal in this.triangleDiagonals)
            {
                this.theCanvas.Children.Remove(triangleDiagonal);
            }
            this.triangleDiagonals.Clear();
        }

        private void MosaicTypeSelect_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.mosaicTypeChanged = true;
            switch (this.mosaicTypeSelect.SelectedIndex)
            {
                case 1:
                    this.currentMosaicType = this.validMosaicTypes[1];
                    this.openPaletteFilesButton.IsEnabled = false;
                    this.imagePaletteList.IsEnabled = false;
                    this.addToPalette.IsEnabled = false;
                    this.clearPalette.IsEnabled = false;
                    break;
                case 2:
                    this.currentMosaicType = this.validMosaicTypes[2];
                    this.openPaletteFilesButton.IsEnabled = true;
                    this.imagePaletteList.IsEnabled = true;
                    if (this.imagePaletteList.ItemsSource != null)
                    {
                        this.addToPalette.IsEnabled = true;
                        this.clearPalette.IsEnabled = true;
                    }
                    break;
            }
        }

        private async void GenerateMosaicButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Window.Current.CoreWindow.PointerCursor = new CoreCursor(CoreCursorType.Wait, 1);
                switch (this.mosaicTypeSelect.SelectedIndex)
                {
                    case 1:
                        this.mosaic = new SolidBlockMosaic(
                            this.sourceWriteableBitmap,
                            this.sourceImageBytes,
                            this.blockSize,
                            this.SourceFileImage);
                        break;
                    case 2:

                        this.mosaic = new PictureMosaic(
                            this.sourceWriteableBitmap,
                            this.sourceImageBytes,
                            this.blockSize,
                            this.SourceFileImage,
                            this.paletteImages,
                            this.paletteImageBytes);

                        break;
                }

                this.setIfTriangleMosaic();

                this.mosaicImage = await this.mosaic.GetMosaic();
                this.mosaicOutput.Source = this.mosaicImage;
                this.initializeChangedBools();
                this.isFirstTimeRunning = false;
                this.saveMosaicButton.IsEnabled = true;

                this.mosaicWithZoom.Source = this.mosaicImage;
                this.mosaicWithZoom.Height = this.sourceWriteableBitmap.PixelHeight;
                this.mosaicWithZoom.Width = this.sourceWriteableBitmap.PixelWidth;
                this.mosaicZoomCheckBox.Visibility = Visibility.Visible;
                this.refreshMosaicZoomImage();
                Window.Current.CoreWindow.PointerCursor = new CoreCursor(CoreCursorType.Arrow, 1);
            }
            catch (Exception)
            {
                await new MessageDialog("Please select a source image first.").ShowAsync();
                this.mosaicTypeSelect.SelectedIndex = 0;
            }
        }

        private void setIfTriangleMosaic()
        {
            var triangleChecked = this.triangleCheckBox.IsChecked;
            if (triangleChecked != null)
            {
                this.mosaic.IsTriangle = (bool) triangleChecked;
            }

            var switchedChecked = this.switchDirectionCheckBox.IsChecked;
            if (switchedChecked != null)
            {
                this.mosaic.IsLeftToRightDiagnalLine = (bool) switchedChecked;
            }
        }

        private void ClearPalette_OnClick(object sender, RoutedEventArgs e)
        {
            this.imagePaletteList.ItemsSource = null;
            this.addToPalette.IsEnabled = false;
            this.clearPalette.IsEnabled = false;
            this.numberOfPaletteImagesTextBlock.Text = "0 Images.";
        }

        private async void AddToPalette_OnClick(object sender, RoutedEventArgs e)
        {
            await this.addPicturesToPalette(new List<StorageFile>());
            this.numberOfPaletteImagesTextBlock.Text = this.numberOfPaletteImages + " Images.";
        }
        
        private void ZoomCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var isChecked = this.zoomCheckBox.IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                this.sourceImage.Visibility = Visibility.Collapsed;
                this.sourceScrollView.Visibility = Visibility.Visible;
                this.zoomSlider.Visibility = Visibility.Visible;
            }
            else
            {
                this.sourceImage.Visibility = Visibility.Visible;
                this.sourceScrollView.Visibility = Visibility.Collapsed;
                this.zoomSlider.Visibility = Visibility.Collapsed;
            }
        }

        private void ZoomSlider_OnValueChanged(object sender, RangeBaseValueChangedEventArgs rangeBaseValueChangedEventArgs)
        {
            this.refreshSourceZoom();
        }

        private void refreshSourceZoom()
        {
            var percentZoom = this.zoomSlider.Value / 100;
            var newImage = new Image
            {
                Source = this.sourceImageWithZoom.Source,
                Stretch = Stretch.Uniform,
                Name = "sourceImageWithZoom",
                Height = this.sourceImageWithZoom.Height * percentZoom,
                Width = this.sourceImageWithZoom.Width * percentZoom
            };
            this.sourceScrollView.Content = newImage;
        }

        private void MosaicZoomCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var isChecked = this.mosaicZoomCheckBox.IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                this.mosaicOutput.Visibility = Visibility.Collapsed;
                this.mosaicScrollView.Visibility = Visibility.Visible;
                this.mosaicSlider.Visibility = Visibility.Visible;
            }
            else
            {
                this.mosaicOutput.Visibility = Visibility.Visible;
                this.mosaicScrollView.Visibility = Visibility.Collapsed;
                this.mosaicSlider.Visibility = Visibility.Collapsed;
            }
        }

        private void MosaicSlider_OnValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.refreshMosaicZoomImage();
        }

        private void refreshMosaicZoomImage()
        {
            var percentZoom = this.mosaicSlider.Value / 100;
            var newMosaic = new Image
            {
                Source = this.mosaicWithZoom.Source,
                Stretch = Stretch.Uniform,
                Name = "mosaicWithZoom",
                Height = this.mosaicWithZoom.Height * percentZoom,
                Width = this.mosaicWithZoom.Width * percentZoom
            };
            this.mosaicScrollView.Content = newMosaic;
        }

        private void TriangleCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var isChecked = this.triangleCheckBox.IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                this.switchDirectionCheckBox.IsEnabled = true;
                this.drawGrid();
            }
            else
            {
                this.switchDirectionCheckBox.IsEnabled = false;
                this.switchDirectionCheckBox.IsChecked = false;
                this.removeOldTriangleDiagonals();
            }
            this.gridSizeChanged = true;
        }

        private void SwitchDirectionCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var isChecked = this.switchDirectionCheckBox.IsChecked;
            if (isChecked != null && (bool)isChecked)
            {
                this.drawGrid();
            }
            else
            {
                this.removeOldTriangleDiagonals();
                this.drawGrid();
            }
            this.gridSizeChanged = true;
        }

        #endregion
    }
}