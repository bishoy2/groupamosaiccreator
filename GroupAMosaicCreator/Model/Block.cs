﻿using System.Collections.Generic;
using System.Linq;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     Models a block in a mosaic and contains functionality for finding the average color of said block.
    /// </summary>
    public class Block
    {
        #region Properties

        /// <summary>
        ///     Gets the width of the pixel.
        /// </summary>
        /// <value>
        ///     The width of the pixel.
        /// </value>
        public int PixelWidth { get; private set; }

        /// <summary>
        ///     Gets the height of the pixel.
        /// </summary>
        /// <value>
        ///     The height of the pixel.
        /// </value>
        public int PixelHeight { get; private set; }

        /// <summary>
        ///     Gets or sets the pixels.
        /// </summary>
        /// <value>
        ///     The pixels.
        /// </value>
        public List<Pixel> Pixels { get; set; }

        /// <summary>
        /// Gets or sets the triangles average set as a block.
        /// </summary>
        /// <value>
        /// The triangles average block.
        /// </value>
        public List<Pixel> TrianglesAverageBlock { get; set; }
        
        /// <summary>
        /// Gets or sets the left triangle pixels.
        /// </summary>
        /// <value>
        /// The left triangle pixels.
        /// </value>
        public List<Pixel> LeftTrianglePixels { get; set; }

        /// <summary>
        /// Gets or sets the right triangle pixels.
        /// </summary>
        /// <value>
        /// The right triangle pixels.
        /// </value>
        public List<Pixel> RightTrianglePixels { get; set; }

        /// <summary>
        ///     Gets or sets the average color.
        /// </summary>
        /// <value>
        ///     The average color.
        /// </value>
        public Pixel AverageColor { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Block" /> class.
        /// </summary>
        public Block(int pixelWidth, int pixelHeight)
        {
            this.AverageColor = new Pixel(0, 0, 0, 0);
            this.Pixels = new List<Pixel>();
            this.PixelHeight = pixelHeight;
            this.PixelWidth = pixelWidth;
            this.LeftTrianglePixels = new List<Pixel>();
            this.RightTrianglePixels = new List<Pixel>();
            this.TrianglesAverageBlock = new List<Pixel>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Averages the red.
        /// </summary>
        /// <returns>Average red value.</returns>
        public byte AverageRed()
        {
            var sum = 0;
            this.Pixels.ForEach(p => sum += p.Red);
            return (byte) (sum/this.Pixels.Count);
        }

        /// <summary>
        ///     Averages the green.
        /// </summary>
        /// <returns>Average green value.</returns>
        public byte AverageGreen()
        {
            var sum = 0;
            this.Pixels.ForEach(p => sum += p.Green);
            return (byte) (sum/this.Pixels.Count);
        }

        /// <summary>
        ///     Averages the blue.
        /// </summary>
        /// <returns>Average blue value.</returns>
        public byte AverageBlue()
        {
            var sum = 0;
            this.Pixels.ForEach(p => sum += p.Blue);
            return (byte) (sum/this.Pixels.Count);
        }

        /// <summary>
        /// Gets the average color of a triangle.
        /// </summary>
        /// <param name="trianglePixels">The triangle pixels.</param>
        /// <returns></returns>
        public Pixel GetAverageColorOfTriangle(List<Pixel> trianglePixels)
        {
            var redSum = 0;
            var greenSum = 0;
            var blueSum = 0;

            foreach (var pixel in trianglePixels)
            {
                redSum += pixel.Red;
                greenSum += pixel.Green;
                blueSum += pixel.Blue;
            }
            var pixelCount = trianglePixels.Count;

            var redAverage = (byte)(redSum / pixelCount);
            var greenAverage = (byte)(greenSum / pixelCount);
            var blueAverage = (byte)(blueSum / pixelCount);

            return new Pixel(redAverage, greenAverage, blueAverage, trianglePixels.First().Alpha);
        }

        #endregion
    }
}