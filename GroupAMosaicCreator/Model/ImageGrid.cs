﻿using System;
using System.Collections.Generic;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     A horizontal row of blocks in a Mosaic.
    /// </summary>
    public class ImageGrid
    {
        #region Data members

        /// <summary>
        ///     Gets or sets the blocks.
        /// </summary>
        /// <value>
        ///     The blocks.
        /// </value>
        public List<Block> Blocks;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the number blocks per row.
        /// </summary>
        /// <value>
        ///     The number blocks per row.
        /// </value>
        public int NumberBlocksPerRow { get; set; }

        /// <summary>
        ///     Gets or sets the number blocks per column.
        /// </summary>
        /// <value>
        ///     The number blocks per column.
        /// </value>
        public int NumberBlocksPerColumn { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ImageGrid" /> class.
        /// </summary>
        public ImageGrid()
        {
            this.Blocks = new List<Block>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Builds the grid.
        /// </summary>
        /// <param name="blockSize">Size of the block.</param>
        /// <param name="sourceHeight">Height of the source.</param>
        /// <param name="sourceWidth">Width of the source.</param>
        public void BuildSourceGrid(int blockSize, int sourceHeight, int sourceWidth)
        {
            this.NumberBlocksPerRow = (int) Math.Ceiling(sourceWidth/(double) blockSize);
            this.NumberBlocksPerColumn = (int) Math.Ceiling(sourceHeight/(double) blockSize);

            var checkHeight = 0;
            for (var i = 0; i < this.NumberBlocksPerColumn; i++)
            {
                checkHeight += blockSize;
                var checkWidth = 0;

                this.placeRowOfBlocksInMosaic(blockSize, checkWidth, checkHeight, sourceHeight, sourceWidth);
            }
        }

        private void placeRowOfBlocksInMosaic(int blockSize, int checkWidth, int checkHeight, int sourceHeight, int sourceWidth)
        {
            for (var j = 0; j < this.NumberBlocksPerRow; j++)
            {
                checkWidth += blockSize;

                var currentBlockWidth = this.calculateBlockWidth(blockSize, checkWidth, sourceWidth);

                var currentBlockHeight = this.calculateBlockHeight(blockSize, checkHeight, sourceHeight);

                this.Blocks.Add(new Block(currentBlockWidth, currentBlockHeight));
            }
        }

        private int calculateBlockHeight(int blockSize, int checkHeight, int sourceHeight)
        {
            int currentBlockHeight;
            if (checkHeight > sourceHeight)
            {
                currentBlockHeight = sourceHeight%blockSize;
            }
            else
            {
                currentBlockHeight = blockSize;
            }
            return currentBlockHeight;
        }

        private int calculateBlockWidth(int blockSize, int checkWidth, int sourceWidth)
        {
            int currentBlockWidth;
            if (checkWidth > sourceWidth)
            {
                currentBlockWidth = sourceWidth%blockSize;
            }
            else
            {
                currentBlockWidth = blockSize;
            }
            return currentBlockWidth;
        }

        #endregion
    }
}