﻿namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     A single pixel in a mosaic.
    /// </summary>
    public class Pixel
    {
        #region Properties

        /// <summary>
        ///     Gets the red.
        /// </summary>
        /// <value>
        ///     The red.
        /// </value>
        public byte Red { get; set; }

        /// <summary>
        ///     Gets the blue.
        /// </summary>
        /// <value>
        ///     The blue.
        /// </value>
        public byte Blue { get; set; }

        /// <summary>
        ///     Gets the green.
        /// </summary>
        /// <value>
        ///     The green.
        /// </value>
        public byte Green { get; set; }

        /// <summary>
        ///     Gets the alpha.
        /// </summary>
        /// <value>
        ///     The alpha.
        /// </value>
        public byte Alpha { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Pixel" /> class.
        /// </summary>
        /// <param name="red">The red.</param>
        /// <param name="blue">The blue.</param>
        /// <param name="green">The green.</param>
        /// <param name="alpha">The alpha.</param>
        public Pixel(byte red, byte green, byte blue, byte alpha)
        {
            this.Red = red;
            this.Green = green;
            this.Blue = blue;
            this.Alpha = alpha;
        }

        #endregion
    }
}