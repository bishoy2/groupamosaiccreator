﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using GroupAMosaicCreator.Utilities;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     The Palette of images
    /// </summary>
    /// <seealso cref="GroupAMosaicCreator.Model.Mosaic" />
    public class PictureMosaic : Mosaic
    {
        #region Data members

        private const bool IsPictureMosaic = true;

        private readonly List<WriteableBitmap> paletteImagesThatMatchBlocks;
        private readonly PaletteMatcher paletteMatcher;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PictureMosaic" /> class.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="sourceImageBytes">The source image bytes.</param>
        /// <param name="blockSize">Size of the block.</param>
        /// <param name="sourceFile">The source file.</param>
        /// <param name="paletteImages">The collection of palette images.</param>
        /// <param name="palettePixelBytes">The palette pixel bytes.</param>
        public PictureMosaic(WriteableBitmap sourceImage, byte[] sourceImageBytes, int blockSize, StorageFile sourceFile, List<WriteableBitmap> paletteImages, List<byte[]> palettePixelBytes)
            : base(sourceImage, sourceImageBytes, blockSize, sourceFile)
        {
            Palette = new ImagePalette();
            this.paletteImagesThatMatchBlocks = new List<WriteableBitmap>();

            this.buildPalette(palettePixelBytes);
            this.setAveragePixelOfPaletteImages();

            this.paletteMatcher = new PaletteMatcher {
                MosaicBlocks = MosaicBlocks.Blocks,
                PaletteImages = paletteImages,
                PaletteBlocks = Palette.Blocks
            };
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the mosaic.
        ///     Precondition: none
        ///     Postcondition: return a writableBitmap
        /// </summary>
        /// <returns>
        ///     A mosaic image made up of smaller images.
        /// </returns>
        public override async Task<WriteableBitmap> GetMosaic()
        {
            var collectionOfMatchingImages = this.paletteMatcher.MatchPaletteImageToMosaicBlock();
            this.paletteImagesThatMatchBlocks.AddRange(collectionOfMatchingImages);

            var blocks = await this.resizePaletteBlocks();

            var imagePixels = PixelMapper.MapBlocksToPixels(blocks, IsPictureMosaic);
            var imageBytes = PixelBuilder.ConvertPixelsToByteArray(imagePixels);
            return await ImageConverter.ConvertFileToWritableBitMap(imageBytes, SourceFile);
        }

        private async Task<List<Block>> resizePaletteBlocks()
        {
            var collectionOfMatchingPaletteImages = new List<Block>();

            for (var index = 0; index < this.paletteImagesThatMatchBlocks.Count; index++)
            {
                var image = this.paletteImagesThatMatchBlocks[index];

                var block = MosaicBlocks.Blocks[index];

                var paletteImageBytes =
                    await ImageConverter.ResizeWritableBitmap(image, (uint) block.PixelWidth, (uint) block.PixelHeight);

                var paletteImagePixels = PixelBuilder.BuildPixels(paletteImageBytes);

                block.Pixels.Clear();
                block.Pixels.AddRange(paletteImagePixels);

                collectionOfMatchingPaletteImages.Add(block);
            }
            return collectionOfMatchingPaletteImages;
        }

        private void buildPalette(List<byte[]> collectionOfPaletteBytes)
        {
            foreach (var paletteBytes in collectionOfPaletteBytes)
            {
                var imagePixels = PixelBuilder.BuildPixels(paletteBytes);
                Palette.AddBlock(50, 50, imagePixels);
            }
        }

        private void setAveragePixelOfPaletteImages()
        {
            foreach (var block in Palette.Blocks)
            {
                block.AverageColor.Red = block.AverageRed();
                block.AverageColor.Green = block.AverageGreen();
                block.AverageColor.Blue = block.AverageBlue();
                block.AverageColor.Alpha = block.Pixels.First().Alpha;
            }
        }

        #endregion
    }
}