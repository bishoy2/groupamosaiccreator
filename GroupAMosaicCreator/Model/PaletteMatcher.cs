﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     Matches Palette images to source images blocks
    /// </summary>
    public class PaletteMatcher
    {
        #region Data members

        private const int ColorThreshold = 75;

        private readonly List<WriteableBitmap> collectionOfMatchingPaletteBitmapImages;
        private readonly Random randomizer;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the mosaic blocks.
        /// </summary>
        /// <value>
        ///     The mosaic blocks.
        /// </value>
        public List<Block> MosaicBlocks { get; set; }

        /// <summary>
        ///     Gets or sets the palette images.
        /// </summary>
        /// <value>
        ///     The palette images.
        /// </value>
        public List<WriteableBitmap> PaletteImages { get; set; }

        /// <summary>
        ///     Gets or sets the paletet blocks.
        /// </summary>
        /// <value>
        ///     The palette blocks.
        /// </value>
        public List<Block> PaletteBlocks { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PaletteMatcher" /> class.
        /// </summary>
        public PaletteMatcher()
        {
            this.MosaicBlocks = new List<Block>();
            this.PaletteImages = new List<WriteableBitmap>();
            this.PaletteBlocks = new List<Block>();
            this.collectionOfMatchingPaletteBitmapImages = new List<WriteableBitmap>();
            this.randomizer = new Random();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Matches the palette image to mosaic block.
        ///     Precondition: Mosaic blocks, Palette images, and Palette blocks cannot be empty
        ///     Postcondition: none
        /// </summary>
        /// <returns>A collection of images that match the source images blocks.</returns>
        public List<WriteableBitmap> MatchPaletteImageToMosaicBlock()
        {
            foreach (var mosaicBlock in this.MosaicBlocks)
            {
                var imagesOfSimilarColorToMosaicBlock = new List<WriteableBitmap>();

                var mosaicBlockAverageColor = convertPixelToColor(mosaicBlock.AverageColor);

                var imagesThatMatchBlockColor = this.getImagesThatMatchBlockColor(mosaicBlockAverageColor);
                imagesOfSimilarColorToMosaicBlock.AddRange(imagesThatMatchBlockColor);

                var maxRandomIndex = imagesOfSimilarColorToMosaicBlock.Count;
                var randomIndex = this.randomizer.Next(maxRandomIndex);
                var randomImage = imagesOfSimilarColorToMosaicBlock[randomIndex];
                this.collectionOfMatchingPaletteBitmapImages.Add(randomImage);
            }
            return this.collectionOfMatchingPaletteBitmapImages;
        }

        private List<WriteableBitmap> getImagesThatMatchBlockColor(Color mosaicBlockAverageColor)
        {
            var imagesOfSimilarColorToMosaicBlock = new List<WriteableBitmap>();
            var indexesOfPalletColorsFromClosestColorToMosaic = new Dictionary<int, int>();

            foreach (var paletteBlock in this.PaletteBlocks)
            {
                var paletteBlockAverageColor = convertPixelToColor(paletteBlock.AverageColor);
                var distance = this.colorDifference(mosaicBlockAverageColor, paletteBlockAverageColor);
                var palletIndex = this.PaletteBlocks.IndexOf(paletteBlock);

                if (!indexesOfPalletColorsFromClosestColorToMosaic.ContainsKey(distance))
                {
                    indexesOfPalletColorsFromClosestColorToMosaic.Add(distance, palletIndex); 
                }
                
            }

            var distances = indexesOfPalletColorsFromClosestColorToMosaic.Keys.ToList();
            distances.Sort();
            distances.RemoveRange(5, distances.Count - 5);

            foreach (var distance in distances)
            {
                var index = indexesOfPalletColorsFromClosestColorToMosaic[distance];
                imagesOfSimilarColorToMosaicBlock.Add(this.PaletteImages[index]);
            }

            return imagesOfSimilarColorToMosaicBlock;
        }

        private static Color convertPixelToColor(Pixel pixel)
        {
            var alpha = pixel.Alpha;
            var red = pixel.Red;
            var green = pixel.Green;
            var blue = pixel.Blue;

            return Color.FromArgb(alpha, red, green, blue);
        }

        private int colorDifference(Color color1, Color color2)
        {
            var redDifference = Math.Abs(color1.R - color2.R);
            var greenDifference = Math.Abs(color1.G - color2.G);
            var blueDifference = Math.Abs(color1.B - color2.B);

            var distance = (int) Math.Sqrt(redDifference * redDifference
                                           + greenDifference * greenDifference
                                           + blueDifference * blueDifference);

            return distance;
        }

        #endregion
    }
}