﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using GroupAMosaicCreator.Utilities;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     A mosaic made of blocks of color
    /// </summary>
    /// <seealso cref="GroupAMosaicCreator.Model.Mosaic" />
    public class SolidBlockMosaic : Mosaic
    {
        #region Data Members

        private const bool IsPictureMosaic = false;
        private double pixelVector;
        private double leftQuantity;
        private double rightQuantity;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SolidBlockMosaic" /> class.
        /// </summary>
        /// <param name="sourceImage">The source image.</param>
        /// <param name="sourceImageBytes">The source image bytes.</param>
        /// <param name="blockSize">Size of the block.</param>
        /// <param name="sourceFile">The source file.</param>
        public SolidBlockMosaic(WriteableBitmap sourceImage, byte[] sourceImageBytes, int blockSize,
            StorageFile sourceFile) : base(sourceImage, sourceImageBytes, blockSize, sourceFile)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the mosaic.
        /// </summary>
        /// <returns>
        ///     The mosaic image in bytes
        /// </returns>
        public override async Task<WriteableBitmap> GetMosaic()
        {

            var pixels = this.setSolidBlockOrTriangleBlockPixels();
            var imageBytes = PixelBuilder.ConvertPixelsToByteArray(pixels);
            return await ImageConverter.ConvertFileToWritableBitMap(imageBytes, SourceFile);
        }

        private List<Pixel> setSolidBlockOrTriangleBlockPixels()
        {
            if (IsTriangle)
            {
                this.createBlockFromTriangles();
                return PixelMapper.MapTrianglesToPixels(MosaicBlocks.Blocks);
            }
            else
            {
                return PixelMapper.MapBlocksToPixels(MosaicBlocks.Blocks, IsPictureMosaic);
            }
        }

        private void createBlockFromTriangles()
        {
            foreach (var block in MosaicBlocks.Blocks)
            {
                var width = Convert.ToDouble(block.PixelWidth);
                var height = Convert.ToDouble(block.PixelHeight);

                this.setTriangle(block, height, width);
            }
        }

        private void setTriangle(Block block, double height, double width)
        {
            var leftTrianglePixels = new List<Pixel>(block.LeftTrianglePixels);
            var rightTrianglePixels = new List<Pixel>(block.RightTrianglePixels);

            var leftTriangleAverageColor = block.GetAverageColorOfTriangle(leftTrianglePixels);
            var rightTriangleAverageColor = block.GetAverageColorOfTriangle(rightTrianglePixels);

            this.initializeQuantitiesAndVector(height, width);

            for (var pixelRow = 0; pixelRow < height; pixelRow++)
            {
                var quantityOfAverageLeftTrianglePixels = this.getQuantityOfAveragePixels(leftTriangleAverageColor,
                    this.leftQuantity);
                var quantityOfAverageRightTrianglePixels = this.getQuantityOfAveragePixels(rightTriangleAverageColor,
                    this.rightQuantity);

                block.TrianglesAverageBlock.AddRange(quantityOfAverageLeftTrianglePixels);
                block.TrianglesAverageBlock.AddRange(quantityOfAverageRightTrianglePixels);

                this.manipulateQuantities();

            }
        }

        private void initializeQuantitiesAndVector(double height, double width)
        {
            this.pixelVector = Math.Round(width / height, 1);

            if (IsLeftToRightDiagnalLine)
            {
                this.leftQuantity = width - this.pixelVector;
                this.rightQuantity = this.pixelVector;
            }
            else
            {
                this.leftQuantity = this.pixelVector;
                this.rightQuantity = width - this.pixelVector;
            }
        }

        private List<Pixel> getQuantityOfAveragePixels(Pixel averageColor, double rowQuantity)
        {
            var pixelSet = new List<Pixel>();
            var rowQuantityAsInteger = Convert.ToInt32(rowQuantity);

            for (var quantity = 0; quantity < rowQuantityAsInteger; quantity++)
            {
                pixelSet.Add(averageColor);
            }

            return pixelSet;
        }

        private void manipulateQuantities()
        {
            if (IsLeftToRightDiagnalLine)
            {
                this.leftQuantity = Math.Round(this.leftQuantity - this.pixelVector, 1);
                this.rightQuantity = Math.Round(this.rightQuantity + this.pixelVector, 1);
            }
            else
            {
                this.leftQuantity = Math.Round(this.leftQuantity + this.pixelVector, 1);
                this.rightQuantity = Math.Round(this.rightQuantity - this.pixelVector, 1);
            }
        }

        #endregion
    }
}