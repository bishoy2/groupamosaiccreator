﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using GroupAMosaicCreator.Utilities;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     Models the basic behavior of a Mosaic.
    /// </summary>
    public abstract class Mosaic
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the mosaic blocks.
        /// </summary>
        /// <value>
        ///     The mosaic blocks.
        /// </value>
        protected ImageGrid MosaicBlocks { get; set; }

        /// <summary>
        ///     Gets or sets the palette.
        /// </summary>
        /// <value>
        ///     The palette.
        /// </value>
        protected ImagePalette Palette { get; set; }

        /// <summary>
        ///     Gets or sets the palette pixel bytes.
        /// </summary>
        /// <value>
        ///     The palette pixel bytes.
        /// </value>
        protected List<byte[]> PalettePixelBytes { get; set; }

        /// <summary>
        ///     Gets or sets the source file.
        /// </summary>
        /// <value>
        ///     The source file.
        /// </value>
        protected StorageFile SourceFile { get; set; }

        /// <summary>
        ///     Gets or sets the map pixels.
        /// </summary>
        /// <value>
        ///     The map pixels.
        /// </value>
        protected PixelMapper PixelMapper { get; set; }

        /// <summary>
        ///     Gets or sets the width of the source.
        /// </summary>
        /// <value>
        ///     The width of the source.
        /// </value>
        protected int SourceWidth { get; set; }

        /// <summary>
        ///     Gets or sets the height of the source.
        /// </summary>
        /// <value>
        ///     The height of the source.
        /// </value>
        protected int SourceHeight { get; set; }

        /// <summary>
        ///     Gets or sets the number blocks per row.
        /// </summary>
        /// <value>
        ///     The number blocks per row.
        /// </value>
        protected int NumberBlocksPerRow { get; set; }

        /// <summary>
        ///     Gets or sets the number blocks per column.
        /// </summary>
        /// <value>
        ///     The number blocks per column.
        /// </value>
        protected int NumberBlocksPerColumn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is left to right diagnal line (True by default).
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is left to right diagnal line; otherwise, <c>false</c>.
        /// </value>
        public bool IsLeftToRightDiagnalLine { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is triangle (False by default).
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is triangle; otherwise, <c>false</c>.
        /// </value>
        public bool IsTriangle { get; set; } = false;

        #endregion

        #region Data Members

        private double pixelVector;

        private double leftQuantity;

        private double rightQuantity;

        #endregion

        #region Constructors

        /// <summary>
        ///     Base constructor
        /// </summary>
        protected Mosaic(WriteableBitmap sourceImage, byte[] imageBytes, int blockSize, StorageFile sourceFile)
        {
            this.SourceFile = sourceFile;
            this.MosaicBlocks = new ImageGrid();
            var writeableBitmap = sourceImage;

            this.SourceHeight = writeableBitmap.PixelHeight;
            this.SourceWidth = writeableBitmap.PixelWidth;

            this.Initialize(blockSize, imageBytes);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the Mosaic using the given block size.
        /// </summary>
        /// <param name="blockSize">Size of the block.</param>
        /// <param name="imageBytes">The image bytes.</param>
        public void Initialize(int blockSize, byte[] imageBytes)
        {
            this.MosaicBlocks.BuildSourceGrid(blockSize, this.SourceHeight, this.SourceWidth);
            this.NumberBlocksPerRow = this.MosaicBlocks.NumberBlocksPerRow;
            this.NumberBlocksPerColumn = this.MosaicBlocks.NumberBlocksPerColumn;

            this.PixelMapper = new PixelMapper(
                this.NumberBlocksPerRow,
                this.NumberBlocksPerColumn,
                this.SourceWidth,
                this.SourceHeight);

            this.PixelMapper.MapPixelsToBlocks(imageBytes, this.MosaicBlocks.Blocks);

            this.setAveragePixel();

            this.createTriangles();

        }

        private void setAveragePixel()
        {
            foreach (var block in this.MosaicBlocks.Blocks)
            {
                block.AverageColor.Red = block.AverageRed();
                block.AverageColor.Green = block.AverageGreen();
                block.AverageColor.Blue = block.AverageBlue();
                block.AverageColor.Alpha = block.Pixels[0].Alpha;
            }
        }
        
        private void createTriangles()
        {
            foreach (var block in this.MosaicBlocks.Blocks)
            {
                var height = Convert.ToDouble(block.PixelHeight);
                var width = Convert.ToDouble(block.PixelWidth);

                this.setTriangles(width, height, block);
            }
        }

        private void setTriangles(double width, double height, Block block)
        {
            var pixelBlock = new List<Pixel>(block.Pixels);

            this.initializeQuantitiesAndVector(width, height);

            for (var pixelRow = 0; pixelRow < height; pixelRow++)
            {
                this.moveQuantityOfPixelsFormBlockToTriangles(block, pixelBlock);

                this.manipulateQuantities();

                this.quantityMinCheck();
                this.quantityMaxCheck(block.PixelWidth);
            }
        }
        
        private void initializeQuantitiesAndVector(double width, double height)
        {
            this.pixelVector = Math.Round(width / height, 1);

            if (this.IsLeftToRightDiagnalLine)
            {
                this.leftQuantity = width - this.pixelVector;
                this.rightQuantity = this.pixelVector;
            }
            else
            {
                this.leftQuantity = this.pixelVector;
                this.rightQuantity = width - this.pixelVector;
            }
        }

        private void manipulateQuantities()
        {
            if (this.IsLeftToRightDiagnalLine)
            {
                this.leftQuantity = Math.Round(this.leftQuantity - this.pixelVector, 1);
                this.rightQuantity = Math.Round(this.rightQuantity + this.pixelVector, 1);
            }
            else
            {
                this.leftQuantity = Math.Round(this.leftQuantity + this.pixelVector, 1);
                this.rightQuantity = Math.Round(this.rightQuantity - this.pixelVector, 1);
            }
        }

        private void quantityMinCheck()
        {
            if (this.leftQuantity < 0)
            {
                this.leftQuantity = 0;
            }

            if (this.rightQuantity < 0)
            {
                this.rightQuantity = 0;
            }
        }

        private void quantityMaxCheck(int blockWidth)
        {
            if (this.leftQuantity > blockWidth)
            {
                this.leftQuantity = blockWidth;
            }

            if (this.rightQuantity > blockWidth)
            {
                this.rightQuantity = blockWidth;
            }
        }

        private void moveQuantityOfPixelsFormBlockToTriangles(Block block, List<Pixel> pixelBlock)
        {
            var intergerLeftQuantity = Convert.ToInt32(this.leftQuantity);
            var intergerRightQuantity = Convert.ToInt32(this.rightQuantity);

            var leftPixelRange = pixelBlock.GetRange(0, intergerLeftQuantity);
            pixelBlock.RemoveRange(0, intergerLeftQuantity);

            var rightPixelRange = pixelBlock.GetRange(0, intergerRightQuantity);
            pixelBlock.RemoveRange(0, intergerRightQuantity);

            block.LeftTrianglePixels.AddRange(leftPixelRange);
            block.RightTrianglePixels.AddRange(rightPixelRange);
        }

        /// <summary>
        ///     Gets the mosaic.
        /// </summary>
        /// <returns>The mosaic image in bytes</returns>
        public abstract Task<WriteableBitmap> GetMosaic();

        #endregion
    }
}