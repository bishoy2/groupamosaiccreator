﻿using System.Collections.Generic;

namespace GroupAMosaicCreator.Model
{
    /// <summary>
    ///     Palette image blocks
    /// </summary>
    public class ImagePalette
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the blocks.
        /// </summary>
        /// <value>
        ///     The blocks.
        /// </value>
        public List<Block> Blocks { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ImagePalette" /> class.
        /// </summary>
        public ImagePalette()
        {
            this.Blocks = new List<Block>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds a block image to palette with pixels.
        /// </summary>
        /// <param name="pixelWidth">Width of the pixel.</param>
        /// <param name="pixelHeight">Height of the pixel.</param>
        /// <param name="imagePixels">The image pixels.</param>
        public void AddBlock(int pixelWidth, int pixelHeight, List<Pixel> imagePixels)
        {
            var block = new Block(pixelWidth, pixelHeight);

            block.Pixels.AddRange(imagePixels);

            this.Blocks.Add(block);
        }

        #endregion
    }
}