﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace GroupAMosaicCreator.Utilities
{
    /// <summary>
    ///     Converts image types to and from types that can be manipulated.
    /// </summary>
    public static class ImageConverter
    {
        #region Methods

        /// <summary>
        ///     Converts the file to writable bit map.
        ///     Precondition: none
        ///     Postcondition: none
        /// </summary>
        /// <param name="sourcePixels">The source pixels.</param>
        /// <param name="imageFile">The source file.</param>
        /// <returns>
        ///     A writable bitmap
        /// </returns>
        public static async Task<WriteableBitmap> ConvertFileToWritableBitMap(byte[] sourcePixels, StorageFile imageFile)
        {
            var decoder = await ConvertFileToBitmapDecoder(imageFile);

            var writableBitmap = new WriteableBitmap((int) decoder.PixelWidth, (int) decoder.PixelHeight);

            using (var writeStream = writableBitmap.PixelBuffer.AsStream())
            {
                await writeStream.WriteAsync(sourcePixels, 0, sourcePixels.Length);
                return writableBitmap;
            }
        }

        /// <summary>
        ///     Converts the file to writable bit map.
        ///     Precondition: none
        ///     Postcondition: none
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        /// <returns>A writable bitmap</returns>
        public static async Task<WriteableBitmap> ConvertFileToWritableBitMap(StorageFile imageFile)
        {
            var imageProperties = await imageFile.Properties.GetImagePropertiesAsync();
            var writeableBitmap = new WriteableBitmap((int) imageProperties.Width, (int) imageProperties.Height);
            await writeableBitmap.SetSourceAsync(await imageFile.OpenReadAsync());
            return writeableBitmap;
        }

        /// <summary>
        ///     Converts the file to bitmap decoder.
        ///     Precondition: none
        ///     Postcondition: none
        /// </summary>
        /// <param name="imageFile">The image file.</param>
        /// <returns>A bitmap decoder</returns>
        public static async Task<BitmapDecoder> ConvertFileToBitmapDecoder(StorageFile imageFile)
        {
            using (var fileStream = await imageFile.OpenAsync(FileAccessMode.Read))
            {
                return await BitmapDecoder.CreateAsync(fileStream);
            }
        }

        /// <summary>
        ///     Converts the file to byte arrayeach byte represents [A,R,G,B] in that order.
        ///     Precondtion: none
        ///     Postcondition: none
        /// </summary>
        /// <param name="imageFile">The source file.</param>
        /// <returns>A Byte array</returns>
        public static async Task<byte[]> ConvertFileToByteArray(StorageFile imageFile)
        {
            IRandomAccessStream inputstream = await imageFile.OpenReadAsync();
            var newImage = new BitmapImage();
            newImage.SetSource(inputstream);
            var bitmap = newImage;
            var decoder = await ConvertFileToBitmapDecoder(imageFile);
            var transform = new BitmapTransform {
                ScaledWidth = Convert.ToUInt32(bitmap.PixelWidth),
                ScaledHeight = Convert.ToUInt32(bitmap.PixelHeight)
            };

            var pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Bgra8,
                BitmapAlphaMode.Straight,
                transform,
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage
                );

            return pixelData.DetachPixelData();
        }

        /// <summary>
        ///     Resizes the writable bitmap to and array of pixel bytes each byte represents [A,R,G,B] in that order.
        ///     Precondition: none
        ///     Postcondition: none
        /// </summary>
        /// <param name="writeBitmap">The original write bitmap.</param>
        /// <param name="width">The desired image width.</param>
        /// <param name="height">The desired image height.</param>
        /// <returns>A byte array resized to specified dimentions</returns>
        public static async Task<byte[]> ResizeWritableBitmap(WriteableBitmap writeBitmap, uint width, uint height)
        {
            var pixels = await GetPixelsFromWriteableBitmap(writeBitmap);

            using (var inMemoryRandomStream = new InMemoryRandomAccessStream())
            {
                await encodeStream(writeBitmap, inMemoryRandomStream, pixels);

                var transform = getTransform(width, height);

                return await getSourceDecodedPixels(inMemoryRandomStream, transform);
            }
        }

        private static async Task<byte[]> getSourceDecodedPixels(InMemoryRandomAccessStream inMemoryRandomStream,
            BitmapTransform transform)
        {
            inMemoryRandomStream.Seek(0);
            var decoder = await BitmapDecoder.CreateAsync(inMemoryRandomStream);
            var pixelData = await decoder.GetPixelDataAsync(
                BitmapPixelFormat.Rgba8,
                BitmapAlphaMode.Straight,
                transform,
                ExifOrientationMode.IgnoreExifOrientation,
                ColorManagementMode.DoNotColorManage);

            var sourceDecodedPixels = pixelData.DetachPixelData();
            return sourceDecodedPixels;
        }

        private static BitmapTransform getTransform(uint width, uint height)
        {
            var transform = new BitmapTransform {
                ScaledWidth = Convert.ToUInt32(width),
                ScaledHeight = Convert.ToUInt32(height)
            };
            return transform;
        }

        private static async Task encodeStream(WriteableBitmap writeBitmap,
            InMemoryRandomAccessStream inMemoryRandomStream,
            byte[] pixels)
        {
            var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.JpegEncoderId, inMemoryRandomStream);
            encoder.SetPixelData(BitmapPixelFormat.Rgba8, BitmapAlphaMode.Ignore, (uint) writeBitmap.PixelWidth,
                (uint) writeBitmap.PixelHeight, 96, 96, pixels);
            await encoder.FlushAsync();
        }

        /// <summary>
        /// Gets the pixels from writeable bitmap.
        /// </summary>
        /// <param name="writeBitmap">The write bitmap.</param>
        /// <returns></returns>
        public static async Task<byte[]> GetPixelsFromWriteableBitmap(WriteableBitmap writeBitmap)
        {
            byte[] pixels;
            using (var stream = writeBitmap.PixelBuffer.AsStream())
            {
                pixels = new byte[stream.Length];
                await stream.ReadAsync(pixels, 0, pixels.Length);
            }
            return pixels;
        }

        #endregion
    }
}