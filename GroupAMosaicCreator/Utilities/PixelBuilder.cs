﻿using System.Collections.Generic;
using GroupAMosaicCreator.Model;

namespace GroupAMosaicCreator.Utilities
{
    /// <summary>
    ///     Builds a collection of pixels from collection of bytes
    /// </summary>
    public static class PixelBuilder
    {
        #region Methods

        /// <summary>
        ///     Manipulates Pixels from source.
        /// </summary>
        /// <param name="imageBytes">The image bytes.</param>
        /// <returns></returns>
        public static List<Pixel> BuildPixels(byte[] imageBytes)
        {
            var allPixelsInSourceImage = new List<Pixel>();

            for (var i = 0; i < imageBytes.Length; i += 4)
            {
                var red = imageBytes[i];
                var green = imageBytes[i + 1];
                var blue = imageBytes[i + 2];
                var alpha = imageBytes[i + 3];

                var currentPixel = new Pixel(red, green, blue, alpha);

                allPixelsInSourceImage.Add(currentPixel);
            }
            return allPixelsInSourceImage;
        }

        /// <summary>
        ///     Converts the pixels to byte array.
        /// </summary>
        /// <param name="pixels">The pixels.</param>
        /// <returns>A byte array in the form of [R,G,B,A] made up of each byte in a collection of pixels</returns>
        public static byte[] ConvertPixelsToByteArray(List<Pixel> pixels)
        {
            var bytesList = new List<byte>();

            foreach (var pixel in pixels)
            {
                bytesList.Add(pixel.Red);
                bytesList.Add(pixel.Green);
                bytesList.Add(pixel.Blue);
                bytesList.Add(pixel.Alpha);
            }

            return bytesList.ToArray();
        }

        #endregion
    }
}