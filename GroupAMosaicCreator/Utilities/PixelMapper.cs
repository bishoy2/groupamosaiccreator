﻿using System.Collections.Generic;
using System.Linq;
using GroupAMosaicCreator.Model;

namespace GroupAMosaicCreator.Utilities
{
    /// <summary>
    ///     Maps pixels to and from Blocks.
    /// </summary>
    public class PixelMapper
    {
        #region Data members

        private readonly int numberBlocksPerRow;
        private readonly int numberBlocksPerColumn;
        private readonly int sourceWidth;
        private readonly int sourceHeight;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PixelMapper" /> class.
        /// </summary>
        /// <param name="numberBlocksPerRow">The number blocks per row of the source image.</param>
        /// <param name="numberBlocksPerColumn">The number blocks per column of the source image.</param>
        /// <param name="sourceWidth">Width of the source image.</param>
        /// <param name="sourceHeight">Height of the source image.</param>
        public PixelMapper(int numberBlocksPerRow, int numberBlocksPerColumn, int sourceWidth, int sourceHeight)
        {
            this.numberBlocksPerRow = numberBlocksPerRow;
            this.numberBlocksPerColumn = numberBlocksPerColumn;
            this.sourceWidth = sourceWidth;
            this.sourceHeight = sourceHeight;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Maps pixels to blocks.
        /// Precondition: none
        /// Postcondition: bytes will be converted to pixels and placed in blocks.
        /// </summary>
        /// <param name="imageBytes">The image bytes.</param>
        /// <param name="blocks">The blocks.</param>
        public void MapPixelsToBlocks(byte[] imageBytes, List<Block> blocks)
        {
            var pixels = PixelBuilder.BuildPixels(imageBytes);

            var i = 1;
            var startBlockRow = 0;
            var endBlockRow = this.numberBlocksPerRow;

            while (i <= this.sourceHeight)
            {
                var pixelRow = this.getRowOfPixelsAndRemove(pixels);

                var blockRow = blocks.GetRange(startBlockRow, endBlockRow);

                if (this.isLastRowOfPixelsInBlockAndNotLastRowOfBlocks(i, blockRow, blocks.Last().PixelHeight))
                {
                    startBlockRow += endBlockRow;
                }

                placeRowOfPixelsIntoRowOfBlocks(blockRow, pixelRow);
                i++;
            }
        }

        private List<Pixel> getRowOfPixelsAndRemove(List<Pixel> pixels)
        {
            var pixelRow = pixels.GetRange(0, this.sourceWidth);
            pixels.RemoveRange(0, this.sourceWidth);
            return pixelRow;
        }

        private bool isLastRowOfPixelsInBlockAndNotLastRowOfBlocks(int i, List<Block> blockRow,
            int lastBlocksPixelHeight)
        {
            return i%blockRow.First().PixelHeight == 0 && i <= this.sourceHeight - lastBlocksPixelHeight;
        }

        private static void placeRowOfPixelsIntoRowOfBlocks(List<Block> blockRow, List<Pixel> pixelRow)
        {
            foreach (var block in blockRow)
            {
                var pixelSet = pixelRow.GetRange(0, block.PixelWidth);
                pixelRow.RemoveRange(0, block.PixelWidth);

                block.Pixels.AddRange(pixelSet);
            }
        }

        /// <summary>
        /// Gets pixels form blocks and maps them to image. (Pixels are mapped from left to right then top to bottom)
        /// Precondtion: none
        /// Postcondition: none
        /// </summary>
        /// <param name="blocks">The blocks.</param>
        /// <param name="isPictureMosaic">if set to <c>true</c> [is picture mosaic]. else<c>false</c> [is a solid block mosaic]</param>
        /// <returns>A collection of pixels of the mosaic image</returns>
        public List<Pixel> MapBlocksToPixels(List<Block> blocks, bool isPictureMosaic)
        {
            var pixels = new List<Pixel>();

            for (var startBlockRow = 0;
                startBlockRow < (this.numberBlocksPerColumn - 1)*this.numberBlocksPerRow;
                startBlockRow += this.numberBlocksPerRow)
            {
                var blockRow = new List<Block>(blocks.GetRange(startBlockRow, this.numberBlocksPerRow));

                iteratePixelsOfEachRowTopToBottom(blockRow, pixels, isPictureMosaic);
            }
            return pixels;
        }

        private static void iteratePixelsOfEachRowTopToBottom(List<Block> blockRow, List<Pixel> pixels, bool isPictureMosaic)
        {
            var pixelHeight = blockRow.First().PixelHeight;

            for (var startPixelRow = 0; startPixelRow < pixelHeight; startPixelRow++)
            {
                if (isPictureMosaic)
                {
                    assignBlockPixelsToPictureMosaic(blockRow, pixels);
                }
                else
                {
                    assignBlockAverageColorToSolidColorMosaic(blockRow, pixels);
                }
            }
        }

        private static void assignBlockPixelsToPictureMosaic(List<Block> blockRow, List<Pixel> pixels)
        {
            foreach (var block in blockRow)
            {
                var pixelSet = block.Pixels.GetRange(0, block.PixelWidth);
                block.Pixels.RemoveRange(0, block.PixelWidth);
                pixels.AddRange(pixelSet);
            }
        }

        private static void assignBlockAverageColorToSolidColorMosaic(List<Block> blockRow, List<Pixel> pixels)
        {
            foreach (var block in blockRow)
            {
                var pixelSet = new List<Pixel>();
                for (var averagePixelIndex = 0; averagePixelIndex < block.PixelWidth; averagePixelIndex++)
                {
                    pixelSet.Add(block.AverageColor);
                }

                pixels.AddRange(pixelSet);
            }
        }

        /// <summary>
        /// Gets pixels form triangles and maps them to image. (Pixels are mapped from left to right then top to bottom).
        /// Precondition: none
        /// PostCondition: MosaicBlock's TrianglesAverageBlock will be empty. 
        /// </summary>
        /// <param name="blocks">The blocks.</param>
        /// <returns>A collection of pixels of the mosaic image</returns>
        public List<Pixel> MapTrianglesToPixels(List<Block> blocks)
        {
            var pixels = new List<Pixel>();

            for (var startBlockRow = 0;
                startBlockRow < (this.numberBlocksPerColumn - 1) * this.numberBlocksPerRow;
                startBlockRow += this.numberBlocksPerRow)
            {
                var blockRow = new List<Block>(blocks.GetRange(startBlockRow, this.numberBlocksPerRow));

                getPixelsFromBlockRowTopToBottom(blockRow, pixels);
            }

            return pixels;
        }

        private static void getPixelsFromBlockRowTopToBottom(List<Block> blockRow, List<Pixel> pixels)
        {
            var pixelHeight = blockRow.First().PixelHeight;

            for (var startPixelRow = 0; startPixelRow < pixelHeight; startPixelRow++)
            {
                getPixelsFromRowOfEachBlock(blockRow, pixels);
            }
        }

        private static void getPixelsFromRowOfEachBlock(List<Block> blockRow, List<Pixel> pixels)
        {
            foreach (var block in blockRow)
            {
                var pixelSet = block.TrianglesAverageBlock.GetRange(0, block.PixelWidth);
                block.TrianglesAverageBlock.RemoveRange(0, block.PixelWidth);
                pixels.AddRange(pixelSet);
            }
        }

        #endregion
    }
}