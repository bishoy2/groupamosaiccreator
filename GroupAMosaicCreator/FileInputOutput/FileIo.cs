﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml.Media.Imaging;
using GroupAMosaicCreator.Utilities;

namespace GroupAMosaicCreator.FileInputOutput
{
    /// <summary>
    ///     File input and output
    /// </summary>
    public class FileIo
    {
        #region Properties

        /// <summary>
        ///     Gets the dot per inch x.
        /// </summary>
        /// <value>
        ///     The dot per inch x.
        /// </value>
        public double DotPerInchX { get; private set; }

        /// <summary>
        ///     Gets the dot per inch y.
        /// </summary>
        /// <value>
        ///     The dot per inch y.
        /// </value>
        public double DotPerInchY { get; private set; }

        /// <summary>
        ///     Gets the default source image extention.
        /// </summary>
        /// <value>
        ///     The default source image extention.
        /// </value>
        public string DefaultSourceImageExtention { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Loads an image file.
        /// </summary>
        /// <returns>An Image file.</returns>
        public async Task<StorageFile> LoadImageFile()
        {
            var sourceFile = await this.selectSourceFile();

            this.DefaultSourceImageExtention = sourceFile.FileType;

            return sourceFile;
        }

        /// <summary>
        ///     Loads a collection of image files.
        /// </summary>
        /// <returns>A collection of image files</returns>
        public async Task<IReadOnlyList<StorageFile>> LoadCollectionOfImageFiles()
        {
            return await this.selectCollectionOfSourceFilesFromFolder();
        }

        /// <summary>
        ///     Saves an image file.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="sourceFile">The source file.</param>
        public async void SaveImageFile(byte[] bytes, StorageFile sourceFile)
        {
            var savefile = await this.getSaveFIle();

            if (savefile != null)
            {
                var writeableBitmap = await ImageConverter.ConvertFileToWritableBitMap(bytes, sourceFile);

                using (var writeStream = writeableBitmap.PixelBuffer.AsStream())
                {
                    var pixels = await createWritablePixels(bytes, writeStream, writeableBitmap);

                    var stream = await savefile.OpenAsync(FileAccessMode.ReadWrite);
                    var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);

                    var decoder = await ImageConverter.ConvertFileToBitmapDecoder(sourceFile);

                    encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore,
                        (uint) writeableBitmap.PixelWidth,
                        (uint) writeableBitmap.PixelHeight, decoder.DpiX, decoder.DpiY, pixels);
                    await encoder.FlushAsync();

                    stream.Dispose();
                }
            }
        }

        private static async Task<byte[]> createWritablePixels(byte[] bytes, Stream writeStream, WriteableBitmap writeableBitmap)
        {
            await writeStream.WriteAsync(bytes, 0, bytes.Length);
            var pixelStream = writeableBitmap.PixelBuffer.AsStream();
            var pixels = new byte[pixelStream.Length];
            await pixelStream.ReadAsync(pixels, 0, pixels.Length);
            return pixels;
        }

        private async Task<StorageFile> getSaveFIle()
        {
            var fileSavePicker = new FileSavePicker {
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                SuggestedFileName = "New Mosaic Image",
                DefaultFileExtension = this.DefaultSourceImageExtention
            };
            fileSavePicker.FileTypeChoices.Add("PNG files", new List<string> {".png"});
            fileSavePicker.FileTypeChoices.Add("BMP files", new List<string> {".bmp"});
            fileSavePicker.FileTypeChoices.Add("JPEG files", new List<string> {".jpg"});

            return await fileSavePicker.PickSaveFileAsync();
        }

        private async Task<StorageFile> selectSourceFile()
        {
            var fileOpenPicker = new FileOpenPicker {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };

            fileOpenPicker.FileTypeFilter.Add(".jpg");
            fileOpenPicker.FileTypeFilter.Add(".png");
            fileOpenPicker.FileTypeFilter.Add(".bmp");

            return await fileOpenPicker.PickSingleFileAsync();
        }

        private async Task<IReadOnlyList<StorageFile>> selectCollectionOfSourceFilesFromFolder()
        {
            var folderPicker = new FolderPicker {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary
            };

            folderPicker.FileTypeFilter.Add(".jpg");
            folderPicker.FileTypeFilter.Add(".png");
            folderPicker.FileTypeFilter.Add(".bmp");

            var sourceFolder = await folderPicker.PickSingleFolderAsync();

            return await sourceFolder.GetFilesAsync();
        }

        #endregion
    }
}